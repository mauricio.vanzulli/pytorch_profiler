from datasets import load_dataset


def load_dateset(
    dataset_name: str = "opus_books",
    lang_pair: str = "en-fr",
    train_test_ratio: float = 0.2,
):
    books = load_dataset(dataset_name, lang_pair)
    books = books["train"].train_test_split(test_size=train_test_ratio)
    return books


if __name__ == "__main__":
    books = load_dateset()
