# PyTorch Profiler

This README provides an overview of how to use the PyTorch profiler, a powerful tool for profiling PyTorch functions and operations. The PyTorch profiler allows you to analyze the performance of your code, identify bottlenecks, and optimize your deep learning models.

## Installation

To use the PyTorch profiler, you need to have PyTorch installed. You can install PyTorch using pip:

```shell
pip install torch
```

## Usage

The PyTorch profiler provides a decorator called `pytorch_profile` that can be applied to any PyTorch function. It profiles the function and generates a report of the profiling results. Here's an example of how to use it:

```python
from functools import wraps
from torch.profiler import profile, record_function, ProfilerActivity

def pytorch_profile(
    activities=[ProfilerActivity.CPU],
    record_shapes=False,
    profile_memory=True,
    with_stack=True,
    with_flops=False,
    use_cuda=False,
    sort_by="cpu_time_total",
    group_by_input_shape=False,
    row_limit=None,
):
    """
    Decorator for profiling PyTorch functions using the PyTorch profiler.
    ...
    """

    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            with profile(
                activities=activities,
                record_shapes=record_shapes,
                profile_memory=profile_memory,
                with_stack=with_stack,
                with_flops=with_flops,
                use_cuda=use_cuda,
            ) as prof:
                with record_function(func.__name__):
                    result = func(*args, **kwargs)
            table = prof.key_averages(group_by_input_shape=group_by_input_shape).table(
                sort_by=sort_by,
                row_limit=row_limit,
            )
            print(table)
            return result

        return wrapper

    return decorator
```

To profile a specific function, you can apply the `@pytorch_profile()` decorator to it. For example:

```python
@pytorch_profile()
def my_function(*args, **kwargs):
    ...
```

After applying the decorator, you can call the function as usual, and the profiler will generate a report of the profiling results after the function returns.

## Profiling Options

The `pytorch_profile` decorator accepts several options that control the behavior of the profiler. These options include:

- `activities`: A list of activity groups (CPU, CUDA) to use in profiling.
- `record_shapes`: Save information about operator's input shapes.
- `profile_memory`: Track tensor memory allocation/deallocation.
- `with_stack`: Record source information (file and line number) for the operations.
- `with_flops`: Use a formula to estimate the FLOPs (floating point operations) of specific operators.
- `use_cuda`: Deprecated option. Use `activities` instead.
- `sort_by`: Specify the metric to sort the profiling results by.
- `group_by_input_shape`: Group the profiler results by input shape.
- `row_limit`: Limit the number of rows displayed in the profiling results.

You can modify these options as needed to customize the profiling behavior.

## Profiler output

- Name: The name of the function or operation being profiled.
- Self CPU %: The percentage of CPU time consumed by the function or operation itself.
- Self CPU: The total CPU time consumed by the function or operation itself.
- CPU total %: The percentage of total CPU time consumed by the function or operation, including its children.
- CPU total: The total CPU time consumed by the function or operation, including its children.
- CPU time avg: The average CPU time consumed by each call to the function or operation.
- CPU Mem: The amount of CPU memory consumed by the function or operation.
- Self CPU Mem: The change in CPU memory consumption caused by the function or operation itself.
