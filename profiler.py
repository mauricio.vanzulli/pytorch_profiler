from functools import wraps
from torch.profiler import profile, record_function, ProfilerActivity


def pytorch_profile(
    activities=[ProfilerActivity.CPU],
    record_shapes=False,
    profile_memory=True,
    with_stack=True,
    with_flops=False,
    use_cuda=False,
    sort_by="cpu_time_total",
    group_by_input_shape=False,
    row_limit=None,
):
    """
    Decorator for profiling PyTorch functions using the PyTorch profiler.

    Parameters:
        activities (list): List of activity groups (CPU, CUDA) to use in profiling.
            Supported values: torch.profiler.ProfilerActivity.CPU, torch.profiler.ProfilerActivity.CUDA.
            Default: [torch.profiler.ProfilerActivity.CPU].
        record_shapes (bool): Save information about operator's input shapes. Default: False.
        profile_memory (bool): Track tensor memory allocation/deallocation. Default: True.
        with_stack (bool): Record source information (file and line number) for the ops. Default: True.
        with_flops (bool): Use formula to estimate the FLOPs (floating point operations)
            of specific operators (matrix multiplication and 2D convolution). Default: False.
        use_cuda (bool): Deprecated since version 1.8.1. Use `activities` instead.
        sort_by (str): Specify the metric to sort the profiling results by. Default: "cpu_time_total".
        group_by_input_shape (bool): Group the profiler results by input shape. Default: False.
        row_limit (int): Limit the number of rows displayed in the profiling results.

    Returns:
        decorator: Decorator function that can be applied to a PyTorch function.

    Example usage:
        @pytorch_profile()
        def my_function(*args, **kwargs):
            ...

        Then call the function as usual:
        my_function(*args, **kwargs)

        We will se a report of the profiling results after the function returns.
    """

    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            with profile(
                activities=activities,
                record_shapes=record_shapes,
                profile_memory=profile_memory,
                with_stack=with_stack,
                with_flops=with_flops,
                use_cuda=use_cuda,
            ) as prof:
                with record_function(func.__name__):
                    result = func(*args, **kwargs)
            table = prof.key_averages(group_by_input_shape=group_by_input_shape).table(
                sort_by=sort_by,
                row_limit=row_limit,
            )
            # if logger is not None:
            #     logger.info(table)
            # else:
            print(table)

            return result

        return wrapper

    return decorator
