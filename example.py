from transformers import AutoTokenizer, AutoModelForSeq2SeqLM
from transformers import pipeline
from torch.profiler import profile, record_function, ProfilerActivity

# Import the profiler
from profiler import pytorch_profile


class TranslationModel:
    def __init__(self, checkpoint: str):
        self.checkpoint = checkpoint
        self.tokenizer = AutoTokenizer.from_pretrained(checkpoint)
        self.model = AutoModelForSeq2SeqLM.from_pretrained(checkpoint)
        self.number_of_parameters = sum(
            p.numel() for p in self.model.parameters() if p.requires_grad
        )
        self.translator = pipeline(
            "translation", model=self.model, tokenizer=self.tokenizer
        )

    @pytorch_profile(
        activities=[ProfilerActivity.CPU],
        record_shapes=False,
        profile_memory=True,
        with_stack=True,
        with_flops=True,
        use_cuda=False,
        sort_by="cpu_time_total",
        group_by_input_shape=False,
        row_limit=5,
    )
    def __call__(self, text: str):
        return self.translator(text)


if __name__ == "__main__":
    checkpoint = "t5-small"

    # Create an instance of the Model class
    model = TranslationModel(checkpoint)

    # Make predictions using the model
    input_text = "translate English to French: Legumes share resources with nitrogen-fixing bacteria."

    output_text = model(input_text)
    print(output_text)
